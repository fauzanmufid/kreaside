<?php
class ManagePemesanan extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function Insert($nama_tabel, $data_tabel){
		$res = $this->db->insert($nama_tabel, $data_tabel);
		return $res;
    }

    public function getdatapesanan(){
    	  $viewartikel = $this->db->query("SELECT * from pemesanan order by waktu_pemesanan desc");
    	  return $viewartikel->result_array();
    }
}