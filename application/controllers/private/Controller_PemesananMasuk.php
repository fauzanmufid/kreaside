<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_PemesananMasuk extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ManagePemesanan');
        $this->load->library('email');
        $this->load->helper('url');
    }

    public function getdatapesanan(){
        /*
        $data = $this->ManagePemesanan->getdatapesanan();
        if(!isset($_SESSION['username'])){
            redirect('private/login_admin_disini/');       
        }
        else{
        */
        // / Header
        $this->load->view("private/header/include");
        $this->load->view("private/header/navtopbar");
        $this->load->view("private/header/navsidebar");
        // / Content
        $datapesanan=$this->ManagePemesanan->getdatapesanan();
        $this->load->view('private/content/pesananmasuk',array( 'datapesanan' =>$datapesanan));
        // / Footer
        $this->load->view("private/footer/include");
        //}
    }

    public function do_insert(){
    
        $email = $_POST['email'];
        $name = $_POST['name'];
        $tipe = $_POST['tipe'];
        $message = $_POST['message'];
        $kontak = $_POST['kontak'];
        $waktu_pemesanan = date("Y-m-d H.i.s");

        //Thumbs
        $filetmp = $_FILES["file_img1"]["tmp_name"];
        //$filetmp = $name."-".$waktu_pemesanan."-".$filetmp;
        $filename = $_FILES["file_img1"]["name"];
        if($filename!=null){
            $filename = $name."_".$waktu_pemesanan."_".$filename;
            $filepath = "./assets/imagepesanan/".$filename;
            move_uploaded_file($filetmp, $filepath);
        }
        
        $filetmp2 = $_FILES["file_img2"]["tmp_name"];
        //$filetmp2 = $name."-".$waktu_pemesanan."-".$filetmp2;
        $filename2 = $_FILES["file_img2"]["name"];
        if($filename2!=null){
            $filename2 = $name."_".$waktu_pemesanan."_".$filename2;
            $filepath2 = "./assets/imagepesanan/".$filename2;
            move_uploaded_file($filetmp2, $filepath2);
        }
        $data_insert =  array(
            'email_pemesan' => $email,
            'nama_pemesan' => $name,
            'tipe_pesanan' => $tipe,
            'deskripsi_pesanan' => $message,
            'path_informasitambahan1' => $filename,
            'path_informasitambahan2' => $filename2,
            'kontak_pemesan' => $kontak,
            'waktu_pemesanan' => $waktu_pemesanan,
            'status_pemesanan' => 'Unconfirmed',
            );

        $res = $this->ManagePemesanan->Insert('pemesanan', $data_insert);
        // Insert data berhasil
        if($res >= 1){
            // Set SMTP Configuration
            $emailConfig = array(
                'protocol'  => 'smtp',
                'smtp_host' => 'mx1.idhostinger.com',
                'smtp_port' => '465',
                'smtp_user' => 'pesan@kreaside.com',
                'smtp_pass' => 'kreaside',
                'charset'   => 'iso-8859-1'
            );
             // Load CodeIgniter Email library
            $this->load->library('email', $emailConfig);
             
            // Sometimes you have to set the new line character for better result
            $this->email->set_newline("\r\n");
            // Set your email information
            $from = array('email' => 'pesan@kreaside.com', 'name' => $name);
            $to = array('pesan@kreaside.com');
            $subject = "Pemesanan Oleh ".$name."_".$waktu_pemesanan;
             
            $message = "Email: ".$email."\r\nNama Pemesan: ".$name."\r\nTipe Pesanan: ".$tipe."\r\nDeskripsi Pesanan: ".$message."\r\nKontak Pemesan: ".$kontak;
            
            // Set email preferences
            $this->email->from($from['email'], $from['name']);
            $this->email->to($to);
             
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->attach($filepath);
            $this->email->attach($filepath2);
            // Ready to send email and check whether the email was successfully sent
             
            if (!$this->email->send()) {
                // Raise error message
                show_error($this->email->print_debugger());

            }
            else {
                // Show success notification or other things here
                echo 'Success to send email';
                redirect('Controller_Pemesanan/pemesanansukses'); 
            }
            
        }

        else{
            redirect('Controller_Pemesanan/pemesanangagal');
        }

    }
}
