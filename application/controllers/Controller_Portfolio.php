<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Portfolio extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

	public function index()
	{
		$this->load->view('public/header/include');
		$this->load->view('public/header/navbar');
		$this->load->view('public/content/portofolio');
		$this->load->view('public/footer/footer_seperator');
		$this->load->view('public/footer/include');
	}
}
