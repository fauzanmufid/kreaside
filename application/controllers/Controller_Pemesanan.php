<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Pemesanan extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

	public function index()
	{
		$this->load->view('public/header/include');
		$this->load->view('public/header/navbar');
		$notifikasi['notif'] = 'kosong';
		$this->load->view('public/content/pemesanan', $notifikasi);
		$this->load->view('public/footer/footer_blank');
		$this->load->view('public/footer/include');
	}

	public function pemesanansukses()
	{
		$this->load->view('public/header/include');
		$this->load->view('public/header/navbar');
		$notifikasi['notif'] = 'sukses';
		$this->load->view('public/content/pemesanan', $notifikasi);
		$this->load->view('public/footer/footer_blank');
		$this->load->view('public/footer/include');
	}

	public function pemesanangagal()
	{
		$this->load->view('public/header/include');
		$this->load->view('public/header/navbar');
		$notifikasi['notif'] = 'gagal';
		$this->load->view('public/content/pemesanan', $notifikasi);
		$this->load->view('public/footer/footer_blank');
		$this->load->view('public/footer/include');
	}
}
