<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Kreaside</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet"> 
    <link href="<?php echo base_url(); ?>assets/css/lightbox.css" rel="stylesheet"> 
	<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">    
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo1.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-57-precomposed.png">
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
        function initialize() 
        {
        var myLatLng = 
         {
            lat: -7.247371, 
            lng: 112.786015
         };
         var mapProp = 
         {
            center: myLatLng,
            zoom:15,
            mapTypeId:google.maps.MapTypeId.ROADMAP
         };
         var map=new google.maps.Map(document.getElementById("map"),mapProp);
         var marker = new google.maps.Marker
         ({
            position: myLatLng,
            map: map,
            animation:google.maps.Animation.BOUNCE
         });
         marker.setMap(map);
         var infowindow = new google.maps.InfoWindow
         ({
            content:"Kami Disini !"
         });
         infowindow.open(map,marker);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<!--/head-->