<section id="about-company" class="padding-top wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <img src="<?php echo base_url(); ?>assets/images/logo1.png" class="margin-bottom" style="width: 120px; height: 100px;">
                    <h1 class="margin-bottom">Tentang Kreaside.com</h1>
                    <p>Was founded in Surabaya, January 2016 and is in the form of a website application company engaged in the idea of applications and games publisher based mobile or web based new ideas that have not been previously realized.<br /> Background we set up this company are many problems where the owner has no idea that a special ability to be able to put his ideas are. And with this company, aims to help owners realize the idea and to help sell his ideas to the developers and consumers.</p>
                </div>
            </div>
        </div>
    </section>
    <!--/#about-company-->

    <section id="company-information" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="about-us">
                    <div class="col-sm-7 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <h2 class="bold">Latar Belakang</h2>
                        <div class="row">
                           
                            <div class="col-sm-7">
                                <p>Banyaknya permasalahan–permasalahan dimana pemilik ide tidak memiliki kemampuan yang khusus untuk dapat merealisasikan ide nya tersebut. <br> <br> Dan dengan adanya perusahaan ini, bertujuan untuk membantu para pelanggan mewujudkan ide mereka.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 wow fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="our-skills">
                            <h2 class="bold">Kemampuan Kami</h2>
                            <div class="single-skill">
                                <h3>Pemograman</h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"  data-transition="80">80%</div>
                                </div>
                            </div>
                            <div class="single-skill">
                                <h3>Design</h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"  data-transition="70">70%</div>
                                </div>
                            </div>
                            <div class="single-skill">
                                <h3>Entertainment</h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"  data-transition="50">50%</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>