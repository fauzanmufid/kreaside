<section id="home-slider">
        <div class="container">
            <div class="row">
                <div class="main-slider">
                    <div class="slide-text">
                        <h1>Prioritas Kami</h1>
                        <p>Mengutamakan kepuasan dari pelanggan dan ketepatan waktu. Kami siap membantu dalam merealisasikan ide-ide maupaun proyek Anda.</p>
                        <a href="<?php echo base_url(); ?>Pemesanan" class="btn btn-common">Pesan</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets/images/home/slider/hill.png" class="slider-hill" alt="slider image">
                    <img src="<?php echo base_url(); ?>assets/images/home/slider/laptop.png" class="slider-house" alt="slider image" style="width:135px; height:120px;">
                    <img src="<?php echo base_url(); ?>assets/images/home/slider/slack.png" class="slider-sun" alt="slider image" style="width:80px; height:84px;">
                    <img src="<?php echo base_url(); ?>assets/images/home/slider/video.png" class="slider-birds1" alt="slider image" style="width:85px; height:85px;">
                    <img src="<?php echo base_url(); ?>assets/images/home/slider/hp.png" class="slider-birds2" alt="slider image" style="width:81px; height:57;">
                </div>
            </div>
        </div>
        <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
    </section>
    <!--/#home-slider-->

     <section id="action" class="responsive">
        <div class="vertical-center">
             <div class="container">
                <div class="row">
                    <div class="action take-tour">
                        <div class="col-sm-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h1 class="title">Cara Kami Bekerja</h1>
                            <p>Menggunakan Metode 3P (Pesan - Proses - Pengiriman).</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#action-->

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <div class="single-service">
                        <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                            <img src="<?php echo base_url(); ?>assets/images/pesan.png" alt="">
                        </div>
                        <h2>Pesan</h2>
                        <p>Buka website kami, kemudian masuk ke halaman Pesan. Isi formulir pemesanan lalu kirimkan.</p>
                    </div>
                </div>
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="single-service">
                        <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                            <img src="<?php echo base_url(); ?>assets/images/proses.png" alt="">
                        </div>
                        <h2>Proses</h2>
                        <p>Setelah mendapatkan notifikasi dari Anda, tim kami akan segera memproses pesanan Anda. Jika disetujui dan Anda cocok dengan harga yang kami tawarkan, masuk ke proses pengerjaan.</p>
                    </div>
                </div>
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
                    <div class="single-service">
                        <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                            <img src="<?php echo base_url(); ?>assets/images/pengiriman.png" alt="">
                        </div>
                        <h2>Pengiriman</h2>
                        <p>Proses pengerjaan selesai sesuai dengan waktu yang ditentukan, kami kembali mengirim notifikasi ke email Anda, paket akan kami kirim seiring dengan biaya yang Anda kirim.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#services-->

    <section id="clients">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="clients text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                        <p><img src="<?php echo base_url(); ?>assets/images/home/clients.png" class="img-responsive" alt=""></p>
                        <h1 class="title" style="margin-left:30px;">Media Partner</h1>
                        <p style="margin-left:30px;">Mereka merupakan partner-partner terbaik kami.</p>
                    </div>
                    <div class="clients-logo wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/images/part1.png" alt="" style="height:50px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/images/part2.jpg" alt="" style="height:50px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/images/part3.png" alt="" style="height:38px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="1200ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="1200ms">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/images/part4.jpg" alt="" style="height:38px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="1500ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="1500ms">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/images/part5.png" alt="" style="height:42px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="1800ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="1800ms">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/images/part6.png" alt="" style="height:48px; width:140px;"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </section>
    <!--/#clients-->