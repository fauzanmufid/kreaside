
<section id="halaman" style="margin-top:-18px;">
    <div class="col-md-6 col-md-offset-3">
            <?php 
             if($notif=='sukses'){ ?>
                <div class="alert alert-success fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>Pesanan anda telah kami terima</h4>
                            <p>Terima kasih telah melakukan pesanan pada kami. Selanjutnya pihak kami akan menghubungi anda untuk informasi lebih lanjut.</p>
                        </div>                        
            <?php }
                else if($notif=='gagal'){ ?>
                    <div class="alert alert-danger fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>Pesanan anda gagal</h4>
                            <p>Maaf pesanan anda tidak berhasil diproses, silahkan anda coba pesan ulang, jika masih tidak berhasil harap hubungi kami.</p>
                        </div>
               <?php } 
            ?>
    </div>
    <div class="col-md-2 col-md-offset-5">
        <div class="contact-form bottom">
            <div class="copyright-text text-center wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" style="margin-top:20px; margin-bottom:50px;">

            <h2>Pesan Sekarang !</h2>
            <form  method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>private/Controller_PemesananMasuk/do_insert">
                <div class="form-group">
                    <input type="email" name="email" class="form-control" required="required" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="text" name="name" class="form-control" required="required" placeholder="Nama">
                </div>
                <div class="form-group">
                    <select class="form-control" id="sel1" name="tipe">
                        <option>Website</option>
                        <option>Mobile Apps</option>
                        <option>Entertainment</option>
                        <option>Logos</option>
                        <option>Design</option>
                        <option>Videos</option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Deskripsi"></textarea>
                </div>   
                <p style="color:black; margin-top:-12px; margin-bottom:5px;">Informasi Tambahan</p>
                <div class="form-group">
                    <input type="file" name="file_img1" class="form-control">
                    <input type="file" name="file_img2" class="form-control">
                </div>
                <div class="form-group">
                    <input type="text" name="kontak" class="form-control"  placeholder="Kontak">
                </div>               
                <div class="form-group">
                    <input type="reset" name="reset" class="btn btn-reset" value="Reset Data">
                    <input type="submit" name="submit" class="btn btn-submit" value="Kirim">
                </div>
            </form>
            </div>
        </div>
    </div>
    </section>