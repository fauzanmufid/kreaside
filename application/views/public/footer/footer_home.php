<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center bottom-separator">
                    <img src="<?php echo base_url(); ?>assets/images/home/under.png" class="img-responsive inline" alt="">
                </div>
                <div class="col-md-4 col-sm-6" style="margin-left:90px;">
                    <div class="testimonial bottom">
                        <h2>Testimoni Pelanggan</h2>
                        <div class="media">
                            <div class="pull-left">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/images/cust1.jpg" alt="" style="height:81px; width:81px; border-radius:100px;"></a>
                            </div>
                            <div class="media-body">
                                <blockquote>Dengan bantuan Kreaside, saya bisa mendapatkan hasil website yang saya mau dengan cepat dan harga yang murah.</blockquote>
                                <h3><a href="#">- Dimas Widdy</a></h3>
                            </div>
                         </div>
                        <div class="media">
                            <div class="pull-left">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/images/cust2.jpg" alt="" style="height:81px; width:81px; border-radius:100px;"></a>
                            </div>
                            <div class="media-body">
                                <blockquote>Pekerjaan seperti design, aplikasi, dan website seperti sudah ditangani oleh ahlinya.</blockquote>
                                <h3><a href="">- Sanindya Lesario</a></h3>
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="contact-info bottom">
                        <h2>Hubungi Kami</h2>
                        <address>
                        E-mail: <a href="mailto:pradipta.ghusti@gmail.com">info@kreaside.com</a> <br> 
                        Phone: +62 856 793 5455 <br> 
                        </address>

                        <h2>Alamat</h2>
                        <address>
                        Jl. Lebak Timur Asri no. 46, <br> 
                        Kenjeran, <br> 
                        Surabaya, Jawa Timur. <br> 
                        Indonesia <br> 
                        </address>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="contact-form bottom">
                        <h2>Lokasi Kreaside</h2>
                       <div id="map" style="width:350px; height:290px; border-radius:25px;"></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Kreaside.com 2016 || Hak Cipta Terpelihara.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->