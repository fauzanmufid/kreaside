<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Kreaside.com 2016 || Hak Cipta Terpelihara.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->