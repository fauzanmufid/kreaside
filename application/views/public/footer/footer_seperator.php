<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center bottom-separator">
                    <img src="<?php echo base_url(); ?>assets/images/home/under.png" class="img-responsive inline" alt="">
                </div>
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Kreaside.com 2016 || Hak Cipta Terpelihara.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>