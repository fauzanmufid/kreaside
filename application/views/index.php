<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Kreaside</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">    
    <link rel="shortcut icon" href="images/logo1.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
        function initialize() 
        {
        var myLatLng = 
         {
            lat: -7.247371, 
            lng: 112.786015
         };
         var mapProp = 
         {
            center: myLatLng,
            zoom:15,
            mapTypeId:google.maps.MapTypeId.ROADMAP
         };
         var map=new google.maps.Map(document.getElementById("map"),mapProp);
         var marker = new google.maps.Marker
         ({
            position: myLatLng,
            map: map,
            animation:google.maps.Animation.BOUNCE
         });
         marker.setMap(map);
         var infowindow = new google.maps.InfoWindow
         ({
            content:"Kami Disini !"
         });
         infowindow.open(map,marker);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<!--/head-->

<body>
	<header id="header">      
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                   <div class="social-icons pull-right">
                        <ul class="nav nav-pills">
                            <li>
                                <a href="https://www.facebook.com/Kreaside-1512375709059070/?fref=ts">
                                <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/kreaside_id/">
                                <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div> 
                </div>
             </div>
        </div>
        <div class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="index.html">
                    	<h1>
                            <div class="wow scaleIn" data-wow-duration="800ms" data-wow-delay="600ms">
                                <img src="images/logo1.png" alt="logo" style="width: 120px; height: 100px;">
                            </div>
                        </h1>
                    </a>
                    
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.html">Halaman Awal</a></li>
                        <li class="active"><a href="aboutus2.html">Tentang Kami</a></li>
                        <!--<li class="dropdown"><a href="#">Pages <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="aboutus.html">About</a></li>
                                <li><a href="aboutus2.html">About 2</a></li>
                                <li><a href="service.html">Services</a></li>
                                <li><a href="pricing.html">Pricing</a></li>
                                <li><a href="contact.html">Contact us</a></li>
                                <li><a href="contact2.html">Contact us 2</a></li>
                                <li><a href="404.html">404 error</a></li>
                                <li><a href="coming-soon.html">Coming Soon</a></li>
                            </ul>
                        </li>-->                    
                        <!--<li class="dropdown"><a href="blog.html">Blog <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="blog.html">Blog Default</a></li>
                                <li><a href="blogtwo.html">Timeline Blog</a></li>
                                <li><a href="blogone.html">2 Columns + Right Sidebar</a></li>
                                <li><a href="blogthree.html">1 Column + Left Sidebar</a></li>
                                <li><a href="blogfour.html">Blog Masonary</a></li>
                                <li><a href="blogdetails.html">Blog Details</a></li>
                            </ul>
                        </li>-->
                        <li class="active"><a href="portfolio.html">Portfolio <!--<i class="fa fa-angle-down"></i>--></a>
                            <!--<ul role="menu" class="sub-menu">
                                <li><a href="portfolio.html">Portfolio Default</a></li>
                                <li><a href="portfoliofour.html">Isotope 3 Columns + Right Sidebar</a></li>
                                <li><a href="portfolioone.html">3 Columns + Right Sidebar</a></li>
                                <li><a href="portfoliotwo.html">3 Columns + Left Sidebar</a></li>
                                <li><a href="portfoliothree.html">2 Columns</a></li>
                                <li><a href="portfolio-details.html">Portfolio Details</a></li>
                            </ul>-->
                        </li>                         
                        <!--<li><a href="shortcodes.html ">Shortcodes</a></li>-->                    
                    </ul>
                </div>
                <div class="search">
                    <form role="form">
                        <i class="fa fa-search"></i>
                        <div class="field-toggle">
                            <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>
    <!--/#header-->

    <section id="home-slider">
        <div class="container">
            <div class="row">
                <div class="main-slider">
                    <div class="slide-text">
                        <h1>Prioritas Kami</h1>
                        <p>Mengutamakan kepuasan dari pelanggan dan ketepatan waktu. Kami siap membantu dalam merealisasikan ide-ide maupaun proyek Anda.</p>
                        <a href="register.html" class="btn btn-common">Pesan</a>
                    </div>
                    <img src="images/home/slider/hill.png" class="slider-hill" alt="slider image">
                    <img src="images/home/slider/laptop.png" class="slider-house" alt="slider image" style="width:135px; height:120px;">
                    <img src="images/home/slider/slack.png" class="slider-sun" alt="slider image" style="width:80px; height:84px;">
                    <img src="images/home/slider/video.png" class="slider-birds1" alt="slider image" style="width:85px; height:85px;">
                    <img src="images/home/slider/hp.png" class="slider-birds2" alt="slider image" style="width:81px; height:57;">
                </div>
            </div>
        </div>
        <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
    </section>
    <!--/#home-slider-->

     <section id="action" class="responsive">
        <div class="vertical-center">
             <div class="container">
                <div class="row">
                    <div class="action take-tour">
                        <div class="col-sm-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h1 class="title">Cara Kami Bekerja</h1>
                            <p>Menggunakan Metode 3P (Pesan - Proses - Pengiriman).</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#action-->

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <div class="single-service">
                        <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                            <img src="images/pesan.png" alt="">
                        </div>
                        <h2>Pesan</h2>
                        <p>Buka website kami, kemudian masuk ke halaman Pesan. Isi formulir pemesanan lalu kirimkan.</p>
                    </div>
                </div>
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="single-service">
                        <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                            <img src="images/proses.png" alt="">
                        </div>
                        <h2>Proses</h2>
                        <p>Setelah mendapatkan notifikasi dari Anda, tim kami akan segera memproses pesanan Anda. Jika disetujui dan Anda cocok dengan harga yang kami tawarkan, masuk ke proses pengerjaan.</p>
                    </div>
                </div>
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
                    <div class="single-service">
                        <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                            <img src="images/pengiriman.png" alt="">
                        </div>
                        <h2>Pengiriman</h2>
                        <p>Proses pengerjaan selesai sesuai dengan waktu yang ditentukan, kami kembali mengirim notifikasi ke email Anda, paket akan kami kirim seiring dengan biaya yang Anda kirim.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#services-->

    <section id="clients">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="clients text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                        <p><img src="images/home/clients.png" class="img-responsive" alt=""></p>
                        <h1 class="title" style="margin-left:30px;">Media Partner</h1>
                        <p style="margin-left:30px;">Mereka merupakan partner-partner terbaik kami.</p>
                    </div>
                    <div class="clients-logo wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                                <a href="#"><img src="images/part1.png" alt="" style="height:50px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                                <a href="#"><img src="images/part2.jpg" alt="" style="height:50px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                                <a href="#"><img src="images/part3.png" alt="" style="height:38px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="1200ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="1200ms">
                                <a href="#"><img src="images/part4.jpg" alt="" style="height:38px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="1500ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="1500ms">
                                <a href="#"><img src="images/part5.png" alt="" style="height:42px; width:130px;"></a>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-2 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="1800ms">
                            <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="1800ms">
                                <a href="#"><img src="images/part6.png" alt="" style="height:48px; width:140px;"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </section>
    <!--/#clients-->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center bottom-separator">
                    <img src="images/home/under.png" class="img-responsive inline" alt="">
                </div>
                <div class="col-md-4 col-sm-6" style="margin-left:90px;">
                    <div class="testimonial bottom">
                        <h2>Testimoni Pelanggan</h2>
                        <div class="media">
                            <div class="pull-left">
                                <a href="#"><img src="images/cust1.jpg" alt="" style="height:81px; width:81px; border-radius:100px;"></a>
                            </div>
                            <div class="media-body">
                                <blockquote>Dengan bantuan Kreaside, saya bisa mendapatkan hasil website yang saya mau dengan cepat dan harga yang murah.</blockquote>
                                <h3><a href="#">- Dimas Widdy</a></h3>
                            </div>
                         </div>
                        <div class="media">
                            <div class="pull-left">
                                <a href="#"><img src="images/cust2.jpg" alt="" style="height:81px; width:81px; border-radius:100px;"></a>
                            </div>
                            <div class="media-body">
                                <blockquote>Pekerjaan seperti design, aplikasi, dan website seperti sudah ditangani oleh ahlinya.</blockquote>
                                <h3><a href="">- Sanindya Lesario</a></h3>
                            </div>
                        </div>   
                    </div> 
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="contact-info bottom">
                        <h2>Hubungi Kami</h2>
                        <address>
                        E-mail: <a href="mailto:pradipta.ghusti@gmail.com">info@kreaside.com</a> <br> 
                        Phone: +62 856 793 5455 <br> 
                        </address>

                        <h2>Alamat</h2>
                        <address>
                        Jl. Lebak Timur Asri no. 46, <br> 
                        Kenjeran, <br> 
                        Surabaya, Jawa Timur. <br> 
                        Indonesia <br> 
                        </address>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="contact-form bottom">
                        <h2>Lokasi Kreaside</h2>
                       <div id="map" style="width:350px; height:290px; border-radius:25px;"></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Kreaside.com 2016 || Hak Cipta Terpelihara.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
</body>
</html>
