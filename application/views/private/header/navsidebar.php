<div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left info">
                            <!--
                            <p>Hello, <?php echo $_SESSION['username']; ?></p>
                        -->
                            
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <!--
                        <?php 
                        if($_SESSION['userlevel']=='3'){ 
                            echo "" ?>
                        <li>
                            <a href="<?php echo base_url()?>private/Datauser/getdatauser">
                                <i class="glyphicon glyphicon-user"></i> <span>Users</span>
                            </a>
                        </li>
                        <li>
                        <a href="<?php echo base_url()?>private/Dynamicnavigation/getallnav">
                                <i class="glyphicon glyphicon-chevron-up"></i> <span>Navigation Editor</span>
                            </a>
                        </li>
                        <li>
                        <a href="<?php echo base_url()?>private/Manageslider/getdataimage">
                                <i class="glyphicon glyphicon-picture"></i> <span>Slider Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>private/Gamesmanagement/games">
                                <i class="fa fa-gamepad"></i> <span>Games Management</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">                                
                                <i class="glyphicon glyphicon-question-sign"></i><span>FAQ Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url()?>private/Faq/getdatafaq"><i class="glyphicon glyphicon-duplicate"></i> FAQ</a></li>
                                <li><a href="<?php echo base_url()?>private/Pertanyaan/getdatapertanyaan"><i class="glyphicon glyphicon-import"></i> Question come in</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">                                
                                <i class="glyphicon glyphicon-paperclip"></i><span>Article Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url()?>private/Editor/article"><i class="glyphicon glyphicon-new-window"></i> New Post</a></li>
                                <li><a href="<?php echo base_url()?>private/Crudartikel/GetArtikel"><i class="glyphicon glyphicon-list-alt"></i> Manage Article</a></li>
                                <li><a href="<?php echo base_url()?>private/controller_category/pilih_artikel"><i class="glyphicon glyphicon-tags"></i> Set Category & Tag</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">                                
                                <i class="glyphicon glyphicon-link"></i><span>Social Medias</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/charts/morris.html"><i class="fa fa-facebook-square"></i> Facebook</a></li>
                                <li><a href="pages/charts/flot.html"><i class="fa fa-twitter-square"></i> Twitter</a></li>
                                <li><a href="pages/charts/inline.html"><i class="fa fa-instagram"></i> Instagram</a></li>
                            </ul>
                        </li>
                             <?php ""
                            ;}                         
                        ?>

                        <?php if($_SESSION['userlevel']=='2'){
                            echo "" ?>
                                <li>
                            <a href="<?php echo base_url()?>private/Dynamicnavigation/dynav">
                                <i class="glyphicon glyphicon-chevron-up"></i> <span>Navigation Editor</span>
                            </a>
                        </li>
                        <li>
                        <a href="<?php echo base_url()?>private/Manageslider/getdataimage">
                                <i class="glyphicon glyphicon-picture"></i> <span>Slider Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>private/Gamesmanagement/games">
                                <i class="fa fa-gamepad"></i> <span>Games Management</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">                                
                                <i class="glyphicon glyphicon-question-sign"></i><span>FAQ Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url()?>private/Faq/getdatafaq"><i class="glyphicon glyphicon-duplicate"></i> FAQ</a></li>
                                <li><a href="<?php echo base_url()?>private/Pertanyaan/getdatapertanyaan"><i class="glyphicon glyphicon-import"></i> Question come in</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">                                
                                <i class="glyphicon glyphicon-paperclip"></i><span>Article Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url()?>private/Editor/article"><i class="glyphicon glyphicon-new-window"></i> New Post</a></li>
                                <li><a href="<?php echo base_url()?>private/Crudartikel/GetArtikel"><i class="glyphicon glyphicon-list-alt"></i> Manage Article</a></li>
                                <li><a href="<?php echo base_url()?>private/controller_category/pilih_artikel"><i class="glyphicon glyphicon-tags"></i> Set Tag</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">                                
                                <i class="glyphicon glyphicon-link"></i><span>Social Medias</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/charts/morris.html"><i class="fa fa-facebook-square"></i> Facebook</a></li>
                                <li><a href="pages/charts/flot.html"><i class="fa fa-twitter-square"></i> Twitter</a></li>
                                <li><a href="pages/charts/inline.html"><i class="fa fa-instagram"></i> Instagram</a></li>
                            </ul>
                        </li>
                            <?php ""
                            ;}                       
                        ?>
                        
                        <?php if($_SESSION['userlevel']=='1'){
                            echo "" ?>
                        <li class="treeview">
                            <a href="#">                                
                                <i class="glyphicon glyphicon-paperclip"></i><span>Article Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url()?>private/Editor/article"><i class="glyphicon glyphicon-new-window"></i> New Post</a></li>
                                <li><a href="<?php echo base_url()?>private/Crudartikel/GetArtikel"><i class="glyphicon glyphicon-list-alt"></i> Manage Article</a></li>
                                <li><a href="<?php echo base_url()?>private/controller_category/pilih_artikel"><i class="glyphicon glyphicon-tags"></i> Set Tag</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">                                
                                <i class="glyphicon glyphicon-link"></i><span>Social Medias</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/charts/morris.html"><i class="fa fa-facebook-square"></i> Facebook</a></li>
                                <li><a href="pages/charts/flot.html"><i class="fa fa-twitter-square"></i> Twitter</a></li>
                                <li><a href="pages/charts/inline.html"><i class="fa fa-instagram"></i> Instagram</a></li>
                            </ul>
                        </li>
                            <?php ""
                            ;}                         
                        ?>
                        -->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        
                    </h1>
                </section>