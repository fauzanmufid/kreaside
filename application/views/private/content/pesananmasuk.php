 <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pesanan Masuk</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class=
                        "dataTables_wrapper form-inline dt-bootstrap" id=
                        "example2_wrapper">


              <table class="table table-hover">                
            <tr>
                <th> Nama Pemesan </th>
                <th> Email </th>
                <th> Tipe Pemesanan</th>
                <th> Deskripsi </th>
                <th> Gambar Informasi Tambahan 1 </th>
                <th> Gambar Informasi Tambahan 1 </th>
                <th> Kontak </th>
                <th> Waktu Pemesanan </th>                
                <th> Developer </th>
                <th> Status </th>
                <th colspan="2">Tindakan</th>
            </tr>

            <?php foreach ($datapesanan as $temp) { ?>
                
            <tr>
                <td> <?php echo $temp['nama_pemesan']; ?></td>
                <td> <?php echo $temp['email_pemesan'] ?></td>
                <td> <?php echo $temp['tipe_pesanan']; ?></td>
                <td> <?php echo $temp['deskripsi_pesanan'];?></td>
                <td> <?php echo $temp['path_informasitambahan1']; ?></td>
                <td> <?php echo $temp['path_informasitambahan2'];?></td>
                <td> <?php echo $temp['kontak_pemesan'];?></td>
                <td> <?php echo $temp['waktu_pemesanan'];?></td>

                <td> <?php echo $temp['status_pemesanan'];?></td>

                <td>
                    <a class="btn btn-block btn-info" value="Edit" href="<?php echo base_url()."private/crudpesanan/do_edit/".$temp['id_pemesanan']; ?>">Edit</a>
                </td>   
                <td>
                    <button id="<?php echo $temp['id_pemesanan']; ?>" class="btn btn-block btn-danger hapus"  data-toggle="modal" data-target="#myModal"> Delete </button>    
                </td>
            </tr>

    <?php   } ?>

        </table>
    </div>
              </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div>
    </section>
  <div class="container">

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 class="modal-title" align="center">Are You Sure Want to Delete This </h2>
        </div>
        <div class="modal-body">
            <?php for ($i=0; $i < 2; $i++) { ?>
                        <br/>       
     <?php      } ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
          <a href=""><button type="button" class="btn btn-outline btn-danger">Yes</button></a>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<script type="text/javascript">
$(".hapus").click(function(){
    id=this.id;
    var url='<?php echo base_url()."private/crudpesanan/do_delete"?>';
    console.log($(".modal-footer a"));
    $(".modal-footer a").attr('href', url+'/'+id);
})
</script>