<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Reservasi | Kreaside</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/animate.min.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">


    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/logo1.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header">      
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                   <div class="social-icons pull-right">
                        <ul class="nav nav-pills">
                            <li>
                                <a href="https://www.facebook.com/Kreaside-1512375709059070/?fref=ts">
                                <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/kreaside_id/">
                                <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div> 
                </div>
             </div>
        </div>
        <div class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="index.html">
                    <h1>
                        <div class="wow scaleIn" data-wow-duration="800ms" data-wow-delay="600ms">
                                <img src="images/logo1.png" alt="logo" style="width: 120px; height: 100px;">
                        </div>
                    </h1>
                    </a>
                    
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.html">Halaman Awal</a></li>
                        <li class="active"><a href="aboutus2.html">Tentang Kami</a></li>
                        <!--<li class="dropdown"><a href="#">Pages <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="aboutus.html">About</a></li>
                                <li><a href="aboutus2.html">About 2</a></li>
                                <li><a href="service.html">Services</a></li>
                                <li><a href="pricing.html">Pricing</a></li>
                                <li><a href="contact.html">Contact us</a></li>
                                <li><a href="contact2.html">Contact us 2</a></li>
                                <li><a href="404.html">404 error</a></li>
                                <li><a href="coming-soon.html">Coming Soon</a></li>
                            </ul>
                        </li>-->                    
                        <!--<li class="dropdown"><a href="blog.html">Blog <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="blog.html">Blog Default</a></li>
                                <li><a href="blogtwo.html">Timeline Blog</a></li>
                                <li><a href="blogone.html">2 Columns + Right Sidebar</a></li>
                                <li><a href="blogthree.html">1 Column + Left Sidebar</a></li>
                                <li><a href="blogfour.html">Blog Masonary</a></li>
                                <li><a href="blogdetails.html">Blog Details</a></li>
                            </ul>
                        </li>-->
                        <li class="active"><a href="portfolio.html">Portfolio <!--<i class="fa fa-angle-down"></i>--></a>
                            <!--<ul role="menu" class="sub-menu">
                                <li><a href="portfolio.html">Portfolio Default</a></li>
                                <li><a href="portfoliofour.html">Isotope 3 Columns + Right Sidebar</a></li>
                                <li><a href="portfolioone.html">3 Columns + Right Sidebar</a></li>
                                <li><a href="portfoliotwo.html">3 Columns + Left Sidebar</a></li>
                                <li><a href="portfoliothree.html">2 Columns</a></li>
                                <li><a href="portfolio-details.html">Portfolio Details</a></li>
                            </ul>-->
                        </li>                         
                        <!--<li><a href="shortcodes.html ">Shortcodes</a></li>-->                    
                    </ul>
                </div>
                <div class="search">
                    <form role="form">
                        <i class="fa fa-search"></i>
                        <div class="field-toggle">
                            <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>
    <!--/#header-->
    <section id="halaman" style="margin-top:-18px;">
    <div class="col-md-2 col-md-offset-5">
        <div class="contact-form bottom">
            <div class="copyright-text text-center wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" style="margin-top:20px; margin-bottom:50px;">
            <h2>Pesan Sekarang !</h2>
            <form id="main-contact-form" name="contact-form" method="post" action="sendemail.php">
                <div class="form-group">
                    <input type="email" name="email" class="form-control" required="required" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="text" name="name" class="form-control" required="required" placeholder="Nama">
                </div>
                <div class="form-group">
                    <select class="form-control" id="sel1">
                        <option>Website</option>
                        <option>Mobile Apps</option>
                        <option>Entertainment</option>
                        <option>Logos</option>
                        <option>Design</option>
                        <option>Videos</option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Deskripsi"></textarea>
                </div>   
                <p style="color:black; margin-top:-12px; margin-bottom:5px;">Informasi Tambahan</p>
                <div class="form-group">
                    <input type="file" class="form-control">
                    <input type="file" class="form-control">
                </div>
                <div class="form-group">
                    <input type="text" name="name" class="form-control" required="required" placeholder="Kontak">
                </div>               
                <div class="form-group">
                    <input type="reset" name="reset" class="btn btn-reset" value="Reset Data">
                    <input type="submit" name="submit" class="btn btn-submit" value="Kirim">
                </div>
            </form>
            </div>
        </div>
    </div>
    </section>

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Triplet 2015. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
</body>
</html>
